/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Appp from './Appp';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Appp);

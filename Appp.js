import React from 'react';
import {Router, Scene, Tabs} from 'react-native-router-flux';
import Reg from './reg';
import Login from './Login';
import Home from './Home';
import Splash from './splash';

export default class Roote extends React.Component {
  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene key="splash" component={Splash} hideNavBar={true} />
          <Scene key="reg" component={Reg} hideNavBar={true} />
          <Scene key="login" component={Login} hideNavBar={true} />
          <Scene key="home" component={Home} hideNavBar={true} />
        </Scene>
      </Router>
    );
  }
}

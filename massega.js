import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Avatar} from 'react-native-elements';

export default class Massegas extends React.Component {
  showavatar(massega) {
    if (massega.id !== 2) {
      return (
        <Avatar
          source={{
            uri:
              'https://www.travelcontinuously.com/wp-content/uploads/2018/04/empty-avatar.png',
          }}
          rounded
        />
      );
    }
  }
  componentDidMount() {}
  render() {
    const massege = this.props.massage;
    const mamassegamy = massege?.id == 2;
    return (
      <View style={Style.styleContenar}>
        {this.showavatar(massege)}

        <View
          style={[
            Style.massegastyle,
            mamassegamy ? Style.massegamyright : Style.massegaleft,
          ]}>
          <Text>{massege.username}</Text>
          <Text>{massege.text}</Text>
        </View>
      </View>
    );
  }
}
const Style = StyleSheet.create({
  styleContenar: {
    flexDirection: 'row',
    padding: '2%',
    flex: 1,
  },
  massegastyle: {
    flexDirection: 'column',
    marginLeft: '10%',
    marginBottom: '10%',
    flex: 1,
    borderRadius: 5,
    padding: 10,
  },
  massegamyright: {
    backgroundColor: '#C3DEE0',
    alignItems: 'flex-end',
  },
  massegaleft: {
    backgroundColor: '#73BBC1',
    alignItems: 'flex-start',
  },
});

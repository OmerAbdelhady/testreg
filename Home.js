import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Keyboard,
  TextInput,
  TouchableHighlight,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Actions} from 'react-native-router-flux';
import {Header} from 'react-native-elements';
import Massega from './massega';
import socketIOClient from 'socket.io-client';
import LottieView from 'lottie-react-native';

const socket = socketIOClient('https://fathomless-taiga-86436.herokuapp.com/');

export default class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      Text: '',
      disable: true,
      massages: [],
      typing: false,
      texttyping: '',
    };
  }
  componentDidMount = () => {
    console.log(this.props);
    socket.on('chat', (data) => {
      let {massages} = this.state;
      const newmassegas = {
        text: data.message,
        username: data.name,
      };
      this.setState({
        typing: false,
        texttyping: '',
      });
      massages.unshift(newmassegas);
      this.setState({massages});
    });
    socket.on('typing', (data) => {
      if (data) {
        console.log(data);
        this.setState({
          typing: true,
          texttyping: data,
        });
        setTimeout(() => {
          this.setState({
            typing: false,
            texttyping: '',
          });
        }, 3000);
      } else {
      }
    });
  };
  pressLogout() {
    AsyncStorage.removeItem('key-token').then(Actions.login());
  }

  Textchat(text) {
    if (text && text.length >= 2) {
      this.setState({
        text,
        disable: false,
      });
    } else {
      this.setState({
        disable: true,
      });
    }
  }
  clicksend = () => {
    socket.emit('chat', {
      name: 'ommmer',
      message: this.state.text,
    });
    this.textinput.clear();
    Keyboard.dismiss();

    // let {massages} = this.state;
    // const newmassegas = {
    //   text: this.state.text,
    //   username: 'omer',
    // };
    // massages.unshift(newmassegas);
    // this.setState({massages});

    // console.log(this.state.massages);
  };
  renderItem = ({item}) => {
    return <Massega massage={item} />;
  };
  typing() {
    if (this.state.typing) {
      return (
        <View style={{flex: 0.1}}>
          <Text style={style.texttyping}>{this.state.texttyping} </Text>
          <LottieView
            source={require('./assest/3759-typing.json')}
            autoPlay
            loop
            style={style.typing}></LottieView>
        </View>
      );
    } else {
      <View />;
    }
  }

  render() {
    const changetext = this.state.disable ? style.Buttonof : style.Buttonon;
    return (
      <View style={{flex: 1, backgroundColor: '#185640'}}>
        <Header
          containerStyle={{height: 90}}
          statusBarProps={{
            backgroundColor: '#000000',
            barStyle: 'light-content',
          }}
          centerComponent={{
            text: 'Chat',
            style: {fontSize: 25, paddingTop: 15},
          }}
          rightComponent={
            <Text style={style.logout} onPress={this.pressLogout}>
              Log out
            </Text>
          }
        />
        <FlatList
          inverted
          data={this.state.massages}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
        <View style={style.ViewChat}>
          <TextInput
            style={style.Text}
            onChangeText={(text) => {
              this.Textchat(text);
            }}
            ref={(text) => {
              this.textinput = text;
            }}
          />
          <TouchableHighlight
            disabled={this.state.disable}
            style={[style.send, changetext]}
            onPress={this.clicksend}>
            <Text style={style.textsend}>Send</Text>
          </TouchableHighlight>
          {this.typing()}
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  text: {},
  logout: {
    alignSelf: 'flex-end',
    fontSize: 20,
    color: 'red',
  },
  ViewChat: {
    height: 50,
    backgroundColor: '#000000',
  },
  send: {
    justifyContent: 'center',
    position: 'absolute',
    bottom: '0%',
    right: '1%',
    width: 75,
    height: 50,
  },
  Text: {
    position: 'absolute',
    left: 0,
    width: '80%',
    backgroundColor: '#fff',
    borderRadius: 7,
  },
  textsend: {
    fontSize: 30,
    alignContent: 'center',
    alignSelf: 'center',
    color: '#fff',
  },
  Buttonon: {
    backgroundColor: '#00575D',
  },
  Buttonof: {
    backgroundColor: '#B3DCE0',
  },
  typing: {
    width: '70%',
    height: '390%',
    position: 'relative',
    left: '40%',
    bottom: '330%',
  },
  texttyping: {
    fontSize: 20,
    position: 'relative',
    left: '60%',
    bottom: '650%',
  },
});

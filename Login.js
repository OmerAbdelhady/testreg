import React from 'react';
import {View, Text, TextInput} from 'react-native';
import axios from 'axios';
// import {AsyncStorage} from 'AsyncStorage';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';

export default class Login extends React.Component {
  presssign() {
    Actions.reg();
  }
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
    };
  }
  username(user) {
    this.setState({
      username: user,
    });
    console.log(this.state.username);
  }
  password(pass) {
    this.setState({
      password: pass,
    });
    console.log(this.state.password);
  }
  Clicksign = () => {
    console.log(
      'the user name is ' +
        this.state.username +
        'password ' +
        this.state.password,
    );
    axios
      .post('https://cryptic-meadow-98283.herokuapp.com/api/users/login', {
        username: this.state.username,
        password: this.state.password,
      })
      .then((res) => this.Loginsecsses(res.data))
      .catch((error) => console.error(error.response.data));
  };
  Loginsecsses = (data) => {
    AsyncStorage.setItem('key-token', data.token).then(() => {
      Actions.home({userhaha: 'omer'});
    });
  };

  render() {
    return (
      <View style={{paddingTop: '10%'}}>
        <TextInput
          placeholder="username"
          onChangeText={(user) => this.username(user)}
        />
        <TextInput
          placeholder="password"
          onChangeText={(pass) => this.password(pass)}
        />
        <Text style={{paddingRight: '40%'}} onPress={() => this.Clicksign()}>
          تسجيل دخول
        </Text>
        <Text
          style={{fontSize: 20, paddingTop: 20, color: 'red'}}
          onPress={this.presssign}>
          تسجيل حساب جديد
        </Text>
      </View>
    );
  }
}

import React from 'react';
import {ActivityIndicator, View, Text, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Actions} from 'react-native-router-flux';
import LottieView from 'lottie-react-native';

export default class Splash extends React.Component {
  componentDidMount() {
    setTimeout(() => {
      AsyncStorage.getItem('key-token').then((token) => {
        if (token) {
          Actions.home();
        } else {
          Actions.login();
        }
      });
    }, 4000);
  }
  render() {
    return (
      <View style={style.splash}>
        <LottieView
          source={require('./assest/17155-splash-mi-cuenta.json')}
          autoPlay
          loop
          style={{marginBottom: '20%'}}
        />
        <LottieView
          source={require('./assest/226-splashy-loader.json')}
          autoPlay
          loop
          style={style.to}
        />
      </View>
    );
  }
}
const style = StyleSheet.create({
  splash: {
    backgroundColor: '#000000',
    flex: 1,
    width: '100%',
    height: '100%',
  },
  to: {
    width: 10,
    height: 100,
    position: 'relative',
    top: '70%',
    left: '31%',
  },
});
